#!/usr/bin/perl

# email-notifier.pl v 0.2
# (c) Alexandr A Alexeev 2012 | http://eax.me/

use strict;
use warnings;
use Mail::POP3Client;
use Date::Parse;
use JSON::XS;
use File::Slurp;
use Getopt::Long;
use IPC::Run qw/start/;

use constant {
    VERSION => "0.2",
    CONFIG => $ENV{HOME}.'/.email_notifier',
  };

#### проверяем наличие всех необходимых утилит ####
{
    my @depends = qw/zenity gpg/;
    my $not_found;
    for(@depends) {
      warn "ERROR: $_ not found\n" and ++$not_found
        if(system("which $_ > /dev/null"));
    }
    exit 1 if($not_found);
}

my %opts;
GetOptions(\%opts, '--no-master-password');

my $json;
if($opts{'no-master-password'}) {
  $json = eval { read_file(CONFIG) };
  if($@) {
    die 'Failed to read '.CONFIG."\n";
  }
} else {
  my $pw = get_password();
  die "get_password() returned undef\n" unless defined $pw;
  $json = decrypt_file(CONFIG, $pw);
  unless(defined $json) {
    message("Invalid password");
    exit 1;
  }
}

my $conf = eval { decode_json($json) };
if($@) {
  die 'Failed to parse '.CONFIG." - invalid JSON.\n";
}

my $err_number = validate_config($conf);
if($err_number > 0) {
  die "Total $err_number error(s).\n";
}

message("email-notifier.pl v".VERSION." is running.,,");

while(1) {
  for my $mailbox (@{$conf->{pop3_list}}) {
    my $pop = new Mail::POP3Client(
	USER     => $mailbox->{user},
	PASSWORD => $mailbox->{password},
	HOST     => $mailbox->{host},
	USESSL   => ($mailbox->{ssl} != 0),
      );

    my $count = $pop->Count;

    if($count < 0) {
      message("$mailbox->{user}: ".$pop->Message);
    } elsif($count > 0) {
      message("$mailbox->{user}: $count new message(s)");
      if($mailbox->{delete} != 0) {
	$pop->Delete($_) for (1 .. $count);
      }
    }
    $pop->Close;
  }
  sleep $conf->{main}{check_interval};
}

sub get_password {
  my $pw = `zenity --entry --text='Введите мастер пароль:' --hide-text --title='email-notifier.pl'`;
  chomp($pw);
  return $? ? undef : $pw;
}

sub decrypt_file {
  my ($fname, $pw) = @_;
  $fname =~ s/'/\\'/g;
  my $h = start(
      ['gpg', '--passphrase-fd', '0', '--no-tty', '--decrypt', $fname], 
      '<pipe', \*IN,
      '>pipe', \*OUT,
      '2>pipe', \*ERR,
    ) or die "start() returned $?";

  print IN "$pw\n";
  close IN; 

  my $data = '';
  while(<OUT>) { $data .= $_ }

  finish $h;
  close OUT;
  close ERR;

  my $rslt = result $h;
  return $rslt > 0 ? undef : $data;
}

sub message {
  my ($msg) = @_;
  $msg =~ s/'/\\'/g;
  system("notify-send -u critical -i info 'Mail Informer:' '$msg' &");
}

sub validate_config {
  my ($json) = @_;
  my $err = 0;
  if(ref($json) eq 'HASH') {
    if(defined $json->{main}) {
      $err += validate_config_main($json->{main});
    } else {
      warn "config - missing 'main' section\n";
      $err++;
    }
    if(defined $json->{pop3_list}) {
      $err += validate_config_pop3_list($json->{pop3_list});
    } else {
      warn "config - missing 'pop3_list' section\n";
      $err++;
    }
  } else {
    warn "config - hash expected\n";
    $err++;
  }
  return $err;
}

sub validate_config_main {
  my ($main) = @_;
  my $err = 0;
  if(ref($main) eq 'HASH') {
    if((defined $main->{check_interval}) && (ref($main->{check_interval}) eq '')) {
      $main->{check_interval} = int $main->{check_interval};
      if($main->{check_interval} <= 0) {
        warn "config.main.check_interval - invalid value\n";
        $err++;
      }
    } else {
      warn "config.main.check_interval - missing or is not a scalar\n";
      $err++;
    }
  } else {
    warn "config.main - hash expected\n";
    $err++;
  }
  return $err;
}

sub validate_config_pop3_list {
  my ($list) = @_;
  my $err = 0;
  if(ref($list) eq 'ARRAY') {
    for my $i (0 .. scalar(@{$list}) - 1) {
      $err += validate_config_pop3_list_entry($list->[$i], $i);
    }
  } else {
    warn "config.pop3_list - list expected\n";
    $err++;
  }
  return $err;
}

sub validate_config_pop3_list_entry {
  my ($entry, $number) = @_;
  my $err = 0;
  if(ref($entry) eq 'HASH') {
    for my $key(qw/user password host/) {
      unless((defined $entry->{$key}) && (ref($entry->{$key}) eq '')) {
        warn "config.main.pop3_list[$number].$key - missing or is not a scalar\n";
        $err++;
      }
    }
    for my $key(qw/ssl delete/) {
      unless((defined $entry->{$key}) && (ref($entry->{$key}) eq '')) {
        $entry->{$key} = 0;
      }
      $entry->{$key} = int($entry->{$key});
    }
  } else {
    warn "config.pop3_list[$number] - hash expected\n";
    $err++;
  }
  return $err;
}
